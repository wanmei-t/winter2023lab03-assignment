import java.util.Scanner;
public class NationalPark
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Samoyed[] samoyedFamily = new Samoyed[4];
		
		for (int i = 0; i < samoyedFamily.length; i++)
		{
			samoyedFamily[i] = new Samoyed();
			System.out.println("What is the age of dog #" + (i+1) + " in years?");
			samoyedFamily[i].age = scan.nextInt();
			scan.nextLine();
			System.out.println("What is the gender of dog #" + (i+1) + "?");
			samoyedFamily[i].gender = scan.nextLine();
			System.out.println("Is dog #" + (i+1) + " hypoallergenic?");
			char answer = scan.nextLine().charAt(0);
			if (answer == 'y') samoyedFamily[i].hypoallergenic = true;
			else if (answer == 'n') samoyedFamily[i].hypoallergenic = false;
			System.out.println();
		}
		
		System.out.print("The last samoyed is ");
		System.out.println(samoyedFamily[3].age + " year(s) old,");
		System.out.println("a "+ samoyedFamily[3].gender + ", and is hypoallergenic: " + samoyedFamily[3].hypoallergenic);
		
		System.out.println();
		
		samoyedFamily[0].bark(0+1);
		samoyedFamily[0].eat();
		
		scan.close();
	}
}