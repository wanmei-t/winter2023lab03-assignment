public class Samoyed
{
	public int age;
	public String gender;
	public boolean hypoallergenic;
	
	public void bark(int n)
	{
		System.out.println("Woof! I am dog #" + n + " of the breed Samoyed and I'm a " + this.gender + ".");
		System.out.print("Woof! I ");
		if (this.hypoallergenic)
		{
			System.out.println("am hypoallergenic. You can hug me!");
		}
		else if (!this.hypoallergenic)
		{
			System.out.println("not hypoallergenic.");
		}
		else
		{
			System.out.println("don't know if I'm hypoallergenic or not.");
		}
	}
	
	public void eat()
	{
		int foodQty = this.age * 10;
		System.out.println("I am " + this.age + " year(s) old so I am going to eat " + foodQty + "g of food for supper!");
	}
}